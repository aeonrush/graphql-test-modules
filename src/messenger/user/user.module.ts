import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";

export const UserModule = SharedModule =>
  new GraphQLModule({
    typeDefs: gql`
      type User {
        online: Boolean!
      }
    `,
    imports: [SharedModule],
    resolvers: {
      User: {
        online: () => true
      }
    }
  });
