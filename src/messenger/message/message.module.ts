import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";
import { UserModule } from "../user/user.module";

export const MessageModule = SharedModule =>
  new GraphQLModule({
    typeDefs: gql`
      interface Message {
        id: String
        user: User
        raw: JSON
      }

      type Query {
        getMessages: [Message]!
      }
    `,
    imports: [SharedModule, UserModule],
    resolvers: {
      Message: {
        __resolveType() {
          // TODO: Replace with right code
          // Dummy function for remove warning
          return "TextMessage";
        }
      },
      Query: {
        getMessages() {
          return [];
        }
      }
    }
  });
