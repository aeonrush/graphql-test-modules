import { GraphQLModule } from "@graphql-modules/core";
import { UserModule } from "./user/user.module";
import { MessageModule } from "./message/message.module";

export const MessengerModule = (SharedModule) =>
  new GraphQLModule({
    imports: [UserModule(SharedModule), MessageModule(SharedModule)]
  });
