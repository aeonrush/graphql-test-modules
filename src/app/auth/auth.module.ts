import { GraphQLModule } from "@graphql-modules/core";
import { Request } from "express";
import gql from "graphql-tag";
import { SharedModule } from "../shared.module";

export const AuthModule = new GraphQLModule({
  typeDefs: gql`
    type Query {
      me: User
    }
    type User {
      id: String
    }
  `,
  resolvers: {
    User: {
      id: user => user._id
    },
    Query: {
      me: (root, args, context) => context.authenticatedUser
    }
  },
  imports: [SharedModule],
  context: async (session: Request) => {
    // const authHeader = session.headers.authorization;
    return {
      ...session,
      test: 123321,
      authenticatedUser: {
        _id: 1,
        username: "me"
      }
    };
  }
});
