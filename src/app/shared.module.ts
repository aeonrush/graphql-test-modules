import { GraphQLModule } from "@graphql-modules/core";
import { ScalarModule } from "./scalar/scalar.module";
import { UserModule } from "./user/user.module";
import { ContextModule } from "./contex.module";

export const SharedModule = new GraphQLModule({
  imports: [ScalarModule, UserModule, ContextModule]
});
