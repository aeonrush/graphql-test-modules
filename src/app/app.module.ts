import { GraphQLModule } from "@graphql-modules/core";
import { AuthModule } from "./auth/auth.module";
import { UserModule } from "./user/user.module";
import { TextMessageModule } from "./text-message/message.module";
import { MessengerModule } from "../messenger/messenger.module";
import { SharedModule } from "./shared.module";

const _MessengerModule = MessengerModule(SharedModule);

export const AppModule = new GraphQLModule({
  context: () => {
    return {
      cool: true
    };
  },
  imports: [
    SharedModule,
    AuthModule,
    _MessengerModule,
    TextMessageModule(_MessengerModule)
  ]
});
