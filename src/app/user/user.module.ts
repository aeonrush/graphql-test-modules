import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";
import { ScalarModule } from "../scalar/scalar.module";

export const UserModule = new GraphQLModule({
  typeDefs: gql`
    type User {
      id: String
      username: String
      createdAt: Date!
    }
  `,
  imports: [ScalarModule],
  resolvers: {
    User: {
      id: user => user._id,
      username(user, args, ctx) {
        return user.username;
      }
    }
  },
  context: () => {
    return {
      UserModule: true
    };
  }
});
