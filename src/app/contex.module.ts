import { GraphQLModule } from "@graphql-modules/core";

export const ContextModule = new GraphQLModule({
  context(currentContext) {
    return currentContext;
  }
});
