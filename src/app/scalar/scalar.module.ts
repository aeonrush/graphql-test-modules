import * as path from "path";
import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";
import { fileLoader, mergeTypes } from "merge-graphql-schemas";
const typesArray = fileLoader(path.join(__dirname, "./graphql"));

export const ScalarModule = new GraphQLModule({
  typeDefs: gql(mergeTypes(typesArray, { all: true, schemaDefinition: false })),
});
