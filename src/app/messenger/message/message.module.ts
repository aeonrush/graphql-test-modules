import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";

export const MessageModule = userModule =>
  new GraphQLModule({
    typeDefs: gql`
      interface Message {
        id: String
        user: User
      }

      type Query {
        getMessages: [Message]!
      }
    `,
    imports: [userModule],
    resolvers: {
      Message: {
        __resolveType() {
          return "TextMessage";
        }
      },
      Query: {
        getMessages(user, args, ctx) {
          console.log(user);
          console.log(ctx);
          return [];
        }
      }
    }
  });
