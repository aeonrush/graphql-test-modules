import { GraphQLModule } from "@graphql-modules/core";
import gql from "graphql-tag";

export const TextMessageModule = (MessengerModule) => new GraphQLModule({
    typeDefs: gql`
      type TextMessage implements Message {
        id: String
        user: User
        raw: JSON
        message: String
      }
    `,
    imports: [MessengerModule]
  });
